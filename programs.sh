#!/bin/sh

# ===============================================================================
# This shell script downloads all necessary programs that are in use in this repo.
# FOR PERSONAL USE ONLY.
# Distributions currently supported: Arch
# ===============================================================================

# List of programs to be downloaded
# ===============================================================================

CORE_PACKAGES=("herbstluftwm" "picom-pijulius-git" "polybar-git" "stow"
"deadd-notification-center-bin" "i3lock-color" "pfetch" "nitrogen" "xorg-server"
"lxappearance" "ttf-iosevka-nerd" "ttf-font-awesome" "otf-font-awesome" "make"
"noto-fonts-main" "network-manager-applet" "xclip" "file-roller" "xorg" "cmake"
"thunar" "thunar-volman" "pavucontrol" "firefox" "balena-etcher" "transgui-qt"
"thunderbird" "gparted" "mousepad" "vlc" "flameshot" "ly" "yt-dlp" "xorg-xinit"
"bluez" "bluez-libs" "bluez-utils" "blueman" "pipewire" "pipewire-alsa" "pipewire-pulse")


# ===============================================================================

# Check if yay is installed
if [[ ! -d "/opt/yay" ]]; then
    echo "It seems that yay is not installed on your system."
    read -p  "Would you like to install it? [y/N]: " yay_choice
    if [ "$yay_choice" == "y" ]; then
      sudo pacman -S base-devel git &&
      cd /opt &&
      sudo git clone https://aur.archlinux.org/yay.git &&
      sudo chown -R $(whoami):users ./yay &&
      cd yay &&
      makepkg -si
    fi
fi

echo "List all the packages that can be installed?"
read -p "[y/N]: " confirm_selection
if [ "$confirm_selection" == "y" ]; then
  echo -e "\n========================================="
  echo -e "\e[1mCORE_PACKAGES:\e[0m\n${CORE_PACKAGES[@]}\n"
  echo -e "========================================="
fi
echo ""


# Ask user what optional packages they want to install
USR_PACKAGES=()
for i in "${CORE_PACKAGES[@]}"; do
  USR_PACKAGES+="$i "
done

# Which package manager to execute the script with
PACKAGE_MGR="yay"

echo -e "${PACKAGE_MGR} -S ${USR_PACKAGES[@]} --noconfirm\n"
echo "Execute this command?"
read -p "[y/N]: " confirm_command
if [ "$confirm_command" == "y" ]; then
  ${PACKAGE_MGR} -S ${USR_PACKAGES[@]} #--noconfirm
else
  echo "Exited the script"
  exit 0
fi
