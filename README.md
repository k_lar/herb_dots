# Automated setup for herbstluftwm

## Core packages in use:

- Window manager: Herbstluftwm
- Notification system: Deadd-linux-notification-center
- Bar: Polybar
- Program launcher: Rofi
- Browser: Firefox
- Terminal: Kitty
- Display / Login manager: LightDM
- Shortcut helper: Showkeys (![](https://github.com/adamharmansky/showkeys))  
- Compositor: Picom

## This script does the following:

1. Asks to install yay (if not present on the system)  
2. Tells the user what packages are going to be installed  
3. User chooses their preferred utilities (terminal, browser)  
4. Installs the packages  
